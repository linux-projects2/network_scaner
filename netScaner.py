#!/usr/bin/env python3
import os
from subprocess import Popen, PIPE
import re
import json
from datetime import datetime as dt

class NetScan():
    """ sudo nmap 192.168.1.1 -O --osscan-guess"""

    def __init__(self):
        """ """

        self.known = {'Netgear Repeater': "80:CC:9C:0F:FE:AC",
                      'Netgear Router': "BC:A5:11:04:1E:E8",
                      'Windows Tablet': "58:CE:2A:CF:A4:BD",
                      'G733': "256",
                      'GW': "00:23:63:A4:C7:07",
                      'Nintendo Switch': "CC:5B:31:B9:64:F3",
                      'Manjaro': "7E:5A:C2:14:B6:3A",
                      'PTZ-1': "F0:00:00:AA:61:B3",
                      'PTZ-2': "F0:00:00:B9:79:67"}

        self.cmd = ['sudo', 'nmap', '-sn', '192.168.1.1/24']
        self.scan = self.map()
        self.devices = self.parse()
        self.log_loc = os.path.join(os.path.dirname(__file__),
                                    'nmap_devices.json')
        # self.log_loc = '/home/my3al/Desktop/netScaner/nmap_devices.json'
        self.log_check()

    def map(self):
        """ """

        with Popen(self.cmd, stdout=PIPE) as scan:
            n_map = scan.communicate()[0].decode()

        return n_map

    def parse(self):
        """ """

        regex = r"^Nmap.*(192\.168\.1\.[0-9]*)\n(.*)\n(.*)$"
        matches = re.finditer(regex, self.scan, re.MULTILINE)
        output = []

        for match in matches:
            device = {}

            for num in range(0, len(match.groups())):
                num = num + 1

                if num == 1:
                    device['ip'] = match.group(num)
                if num == 2:
                    device['latency'] = match.group(num)
                if num == 3:
                    mac_device = match.group(num).split(':',
                                                         maxsplit=1)[1].strip()

                    mac_device = mac_device.strip().split(' ',
                                                           maxsplit=1)

                    device['mac'] = mac_device[0].strip()
                    device['device'] = mac_device[1].strip('()')
                    device['known '] = self.is_known(mac_device[0])
                    device['time'] = dt.now().strftime('%Y-%m-%d | %H:%M:%S')
                    device['old ip'] = []

            output.append(device)

        return output

    def log_check(self):
        """ """

        last_logged = dt.now().strftime('%Y-%m-%d | %H:%M:%S')

        jfile = {}

        if not os.path.exists(self.log_loc):
            json_file = {'start date': last_logged,
                         'router': 'Netgear',
                         'ip': '192.168.1.1',
                         'logging command': self.cmd,
                         'devices': self.devices,
                         'last logged': last_logged}

            with open(self.log_loc, 'w', encoding='utf-8') as _file:
                _file.write(json.dumps(json_file, indent=4))

        with open(self.log_loc, 'r', encoding='utf-8') as jdata:
            jfile = json.loads(jdata.read())

        for device in self.devices:

            macs = [x['mac'] for x in jfile['devices']]

            if device['mac'] in macs or device['mac'] in self.known.values():

                for jdevice in jfile['devices']:
                    if device['mac'] == jdevice['mac']:
                        if device['ip'] != jdevice['ip']:
                            jdevice['old ip'].append(jdevice['ip'])
                            jdevice['ip'] = device['ip']
                            jdevice['time'] = last_logged
                            jdevice['known'] = self.is_known(jdevice['mac'])

            else:
                jfile['devices'].append(device)

        jfile['last logged'] = last_logged

        with open(self.log_loc, 'w', encoding='utf-8') as _file:
            _file.write(json.dumps(jfile, indent=4))


    def is_known(self, mac):
        """ """
        search = [x for x, y in self.known.items() if y == mac]

        if search:
            return search[0]
        return False

if __name__ == '__main__':
    nmap = NetScan()
